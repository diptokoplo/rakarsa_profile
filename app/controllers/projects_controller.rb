class ProjectsController < ApplicationController
  def show
    @project = Project.find(params[:id])
    @project_service = @project.services
    @web_images = @project.images.where(category: "web")
    @tab_images = @project.images.where(category: "tab")
    @phone_images = @project.images.where(category: "phone")
    @header_images = @project.images.where(category: "header")
    @thumbnail_images = @project.images.where(category: "thumbnail")
    
  end
  
  def index 
    @projects = Project.all

  end
  
end
