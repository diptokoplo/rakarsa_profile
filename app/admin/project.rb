ActiveAdmin.register Project do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :client_name, :project_title, :description, :year_id, :type_id, images_attributes: [:id, :name, :name_cache, :category, :_destroy], services_attributes: [:id, :name]
 
form(:html => { :multipart => true }) do |f|
  f.inputs "Project" do
    f.input :client_name
    f.input :project_title
    f.input :description, as: :text
    f.input :year, as: :select, collection: Year.all.map{|u| ["#{u.release_year}", u.id]}
    f.input :type, as: :select, collection: Type.all.map{|u| ["#{u.name}", u.id]}
      f.has_many :images do |ff|
        ff.input :name, :as => :file, input_html: {class: 'file'}, :hint => ff.template.image_tag(ff.object.name.url(:thumb))
        ff.input :name_cache, :as => :hidden 
        ff.input :category, as: :select, collection:  ["header","thumbnail","web","tab","phone"]
      end
      f.has_many :services do |p|
        p.input :name
      end
    f.actions
  end
end



show title: :project_title do
  attributes_table do
  row :id
  row :client_name
  row :project_title
  row :description
  row "Images" do |m|
    project.images.map(&:name).join("<br />").html_safe
    end
  row "Services" do |n|
    project.services.map(&:name).join("<br />").html_safe
    end
  end
end

end
