ActiveAdmin.register Year do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :id, :release_year, project_attributes: [:client_name, :project_title, :description, images_attributes: [:id, :name, :_destroy], services_attributes: [:id, :name]]
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end


end
