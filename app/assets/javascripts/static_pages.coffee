# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#
 $(document).on 'ready page:load', ->
   # Actions to do
   if $('#fullpage-home').length
     $('#fullpage-home').fullpage
       verticalCentered: true
       resize: false
       anchors: [
         ''
         'navigation'
         ]
       css3: true
     return
   else if $('#fullpage-works').length
     $('#fullpage-works').fullpage
       css3: true
       anchors: ['intro', 'secondPage']
       scrollOverflow: true
     return
   else if $('#fullpage-profile').length
     $('#fullpage-profile').fullpage
       css3: true
       setAllowScrolling: false
       anchors: ['intro' , 'secondPage']
       scrollOverflow: true
     return
   else if $('#fullpage-services').length
   	 $('#fullpage-services').fullpage
   	   css3: true
       setAllowScrolling: false
       anchors: ['intro', 'secondPage', 'thirdPage', 'fourthPage']
       scrollOverflow: true
     return
   else if $('#fullpage-contact').length
     $('#fullpage-contact').fullpage
      css3: true
      setAllowScrolling: false
      anchors: ['intro', 'secondPage']
      scrollOverflow: true