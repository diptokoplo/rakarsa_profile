#= require active_admin/base

$(document).on 'has_many_add:after', '.has_many_container', ()->
  $('input.file').change (event) ->
    input = $(event.currentTarget)
    console.log(input)
    file = input[0].files[0]
    console.log(file)
    reader = new FileReader
    preview = $(this).next('p.inline-hints').find('img')
    reader.onload = (e) ->
      image_base64 = e.target.result
      preview.attr 'src', image_base64
      return

    reader.readAsDataURL file
    return
  return

