class Project < ActiveRecord::Base
  belongs_to :type
  belongs_to :year
  has_many :services
  has_many :images
  # attr_accessible :image
  # mount_uploader :image, ImageUploader
  accepts_nested_attributes_for :images, :services, :year, :type, allow_destroy: true
end
