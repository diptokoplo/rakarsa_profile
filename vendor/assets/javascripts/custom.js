jQuery(document).ready(function($){
  var isLateralNavAnimating = false;
  
  //open/close lateral navigation
  $('.cd-nav-trigger').on('click', function(event){
    event.preventDefault();
    //stop if nav animation is running 
    if( !isLateralNavAnimating ) {
      if($(this).parents('.csstransitions').length > 0 ) isLateralNavAnimating = true; 
      
      $('body').toggleClass('navigation-is-open');
      $('.cd-navigation-wrapper').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
        //animation is over
        isLateralNavAnimating = false;
      });
    }
  });
});

;(function ($) {
  'use strict';
  var $body    = $('html, body'), // Define jQuery collection 
      content  = $('#main').smoothState({
        onStart : {
          duration: 5000,
          render: function ($container) {
            content.toggleAnimationClass('is-exiting');
            
            // Scroll user to the top
            $body.animate({ 'scrollTop': 0 });

          }
        }
      }).data('smoothState');
})(jQuery);

// (function ($) {
//     var $window = $(window),
//         $html = $('#mnu');

//     function resize() {
//         if ($window.width() < 1024) {
//             return $html.removeClass('link');
//         }
//         else{
//           $html.addClass('link');
//         }
//     }
// })(jQuery);
$(function(){
    if (window.matchMedia("(max-width: 1024px)").matches) {
      $('#mnu, #mnu1, #mnu2, #mnu3').removeClass('link');
    }
    else {
      $('#mnu, #mnu1, #mnu2, #mnu3').addClass('link');
    }
});