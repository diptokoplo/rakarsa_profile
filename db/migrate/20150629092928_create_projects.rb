class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :client_name
      t.string :project_title
      t.string :description
      t.references :year, index: true
      t.references :type, index: true
      t.timestamps null: false
    end
    add_foreign_key :projects, :types
  end
end
